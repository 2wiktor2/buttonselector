package com.example.buttonselector;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import info.hoang8f.android.segmented.SegmentedGroup;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;
    Button button12;
    Button button24;
    Button button36;

    RadioGroup radioGroup;
    SegmentedGroup segmentedGroup;

    int month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.tv);
        button12 = findViewById(R.id.b_12);
        button24 = findViewById(R.id.b_24);
        button36 = findViewById(R.id.b_36);

        segmentedGroup = findViewById(R.id.segmented2);

        radioGroup = findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button_12:
                        textView.setText("12a");

                        break;
                    case R.id.radio_button_24:
                        textView.setText("24a");

                        break;
                    case R.id.radio_button_36:
                        textView.setText("36a");

                        break;
                }
            }
        });

        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.button21:
                        textView.setText("12b");
                        break;
                    case R.id.button22:
                        textView.setText("24b");
                        break;
                    case R.id.button33:
                        textView.setText("36b");
                        break;
                    case R.id.button44:
                        textView.setText("48b");
                        break;
                }
            }
        });

        button12.setOnClickListener(this);
        button24.setOnClickListener(this);
        button36.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_12:
                textView.setText("12");
                month = 12;
                break;
            case R.id.b_24:
                textView.setText("24");
                month = 24;
                break;
            case R.id.b_36:
                textView.setText("36");
                month = 36;
                break;
        }

    }
}
